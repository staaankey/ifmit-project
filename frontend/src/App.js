import React, { useEffect, useState } from "react";
import "./App.css";
import FirstForm from "./FirstForm";
import SecondForm from "./SecondForm";

function App() {
  useEffect(() => {
    document.title = "Helpfast";
  });

  const [secondForm, setSecondForm] = useState(false);
  const [userId, setUserId] = useState(null);

  return (
    <div className="container">
      <h1 className="text-6xl font-normal leading-normal mt-0 mb-2 text-slate-800">
        Helpfast
      </h1>
      {!secondForm && (
        <FirstForm
          secondForm={secondForm}
          setSecondForm={setSecondForm}
          setUserId={setUserId}
        />
      )}
      {secondForm && <SecondForm userId={userId} />}
    </div>
  );
}

export default App;
