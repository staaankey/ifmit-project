import axios from "axios";

const instance = axios.create({
  withCredentials: false,
  headers: { "Content-Type": "application/json" },
  baseURL: "http://ec2-18-198-55-89.eu-central-1.compute.amazonaws.com:8080/",
});

export const usersAPI = {
  // to test API
  sayHello() {
    return instance.get(`hello`).then((response) => {
      console.log("response", response);
      return response;
    });
  },
  register(values) {
    return instance.post(`register`, values).then((response) => {
      return response;
    });
  },
  create(values) {
    return instance.post("create", values).then((response) => {
      return response;
    });
  },
};
