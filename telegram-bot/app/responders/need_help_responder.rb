class NeedHelpResponder < BasicResponder
  class << self
    def need_help
      current_user.data[:role] = 'need_help'

      inline_message(
        t('need_help.message'),
        [
          [
            InlineButtons::NEED_HELP_PRODUCTS,
            InlineButtons::NEED_HELP_HOUSING
          ],
          [
            InlineButtons::NEED_HELP_GOODS,
            InlineButtons::NEED_HELP_FURNITURE
          ],
          InlineButtons::NEED_HELP_EQUIPMENT,
          InlineButtons::NEED_HELP_ELECTRONICS,
          InlineButtons::NEED_HELP_FINANCIAL,
          InlineButtons::NEED_HELP_CONTACT,
          InlineButtons::BACK_TO_START
        ],
        editless: true
      )
    end

    def where
      current_user.data[:type] = message.data.gsub('need_help_', '')
      text_message(t('need_help_financial.message')) if current_user.data[:type] == 'financial'
      force_reply_message(t('need_help_where.message'))
    end

    def final
      current_user.data[:city] = message.text
      text_message(t('need_help_final.message'))
      text_message(t('final.message'))
    end
  end
end
