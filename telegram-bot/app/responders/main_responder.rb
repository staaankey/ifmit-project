class MainResponder < BasicResponder
  class << self
    def start
      current_user.data = User.column_defaults['data']

      inline_message(
        t('start.message'),
        [
          InlineButtons::NEED_HELP,
          InlineButtons::GIVE_HELP
        ],
        editless: true
      )
    end

    def contact
      text_message(t('not_implemented.message'))
    end
  end
end
