class GiveHelpResponder < BasicResponder
  class << self
    def give_help
      current_user.data[:role] = 'give_help'

      inline_message(
        t('give_help.message'),
        [
          [
            InlineButtons::GIVE_HELP_PRODUCTS,
            InlineButtons::GIVE_HELP_HOUSING
          ],
          [
            InlineButtons::GIVE_HELP_GOODS,
            InlineButtons::GIVE_HELP_FURNITURE
          ],
          InlineButtons::GIVE_HELP_EQUIPMENT,
          InlineButtons::GIVE_HELP_ELECTRONICS,
          InlineButtons::GIVE_HELP_FINANCIAL,
          InlineButtons::GIVE_HELP_CONTACT,
          InlineButtons::BACK_TO_START
        ],
        editless: true
      )
    end

    def where
      current_user.data[:type] = message.data.gsub('give_help_', '')
      force_reply_message(t('give_help_where.message'))
    end

    def whom
      current_user.data[:city] = message.text
      inline_message(
        t('give_help_whom.message'),
        [
          [
            InlineButtons::GIVE_HELP_TO_WAR_VICTIMS,
            InlineButtons::GIVE_HELP_TO_WAR_DISABLE_PEOPLE
          ],
          [
            InlineButtons::GIVE_HELP_TO_CHILDREN,
            InlineButtons::GIVE_HELP_TO_PENSIONERS
          ],
          [
            InlineButtons::GIVE_HELP_TO_ORPHANS,
            InlineButtons::GIVE_HELP_TO_STUDENTS
          ],
          [
            InlineButtons::GIVE_HELP_TO_LARGE_FAMILIES,
            InlineButtons::GIVE_HELP_TO_ALL
          ],
          InlineButtons::BACK_TO_GIVE_HELP_WHERE
        ]
      )
    end

    def final
      current_user.data[:category] = message.data.gsub('give_help_to_', '')

      text_message(t('give_help_final.message'))
      text_message(t('final.message'))
    end
  end
end
