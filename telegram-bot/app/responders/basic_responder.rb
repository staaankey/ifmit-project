class BasicResponder
  extend ResponseHelper

  def self.t(key = nil, throw: false, raise: false, locale: nil, **options)
    I18n.t(key, throw: throw, raise: raise, locale: locale, **options)
  end
end
