module ResponseHelper
  def text_message(message, chat_id = nil)
    chat_id ||= chat_id_from_message

    MessageHandler.bot.api.send_message(
      parse_mode: 'html',
      chat_id: chat_id,
      text: message
    )
  end

  def inline_message(message, keyboard, chat_id = nil, editless: false)
    chat_id ||= chat_id_from_message

    if MessageHandler.callback_query? && editless
      MessageHandler.bot.api.edit_message_text(
        chat_id: chat_id,
        parse_mode: 'html',
        message_id: MessageHandler.message.message.message_id,
        text: message,
        reply_markup: generate_inline_markup(keyboard)
      )
    else
      MessageHandler.bot.api.send_message(
        chat_id: chat_id,
        parse_mode: 'html',
        text: message,
        reply_markup: generate_inline_markup(keyboard)
      )
    end
  end

  def force_reply_message(text, chat_id = nil)
    chat_id ||= chat_id_from_message

    MessageHandler.bot.api.send_message(
      parse_mode: 'html',
      chat_id: chat_id,
      text: text,
      reply_markup: Telegram::Bot::Types::ForceReply.new(
        force_reply: true,
        selective: true
      )
    )
  end

  private

  def current_user
    MessageHandler.current_user
  end

  def message
    MessageHandler.message
  end

  def generate_inline_markup(keyboard)
    Telegram::Bot::Types::InlineKeyboardMarkup.new(
      inline_keyboard: keyboard
    )
  end

  def chat_id_from_message
    if defined?(MessageHandler.message.chat.id)
      MessageHandler.message.chat.id
    else
      MessageHandler.message.message.chat.id
    end
  end
end
