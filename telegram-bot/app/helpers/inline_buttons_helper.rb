module InlineButtonsHelper
  def inline_button(text, callback_data)
    Telegram::Bot::Types::InlineKeyboardButton.new(
      text: text,
      callback_data: callback_data
    ).freeze
  end

  def t(key = nil, throw: false, raise: false, locale: nil, **options)
    I18n.t(key, throw: throw, raise: raise, locale: locale, **options)
  end
end
