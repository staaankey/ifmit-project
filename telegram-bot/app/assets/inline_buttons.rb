module InlineButtons
  extend InlineButtonsHelper

  NEED_HELP = inline_button(t('start.buttons.need_help'), 'need_help')
  GIVE_HELP = inline_button(t('start.buttons.give_help'), 'give_help')
  BACK_TO_START = inline_button(t('shared.buttons.back'), 'main_menu')

  NEED_HELP_PRODUCTS = inline_button(t('help.buttons.products'), 'need_help_products')
  NEED_HELP_HOUSING = inline_button(t('help.buttons.housing'), 'need_help_housing')
  NEED_HELP_GOODS = inline_button(t('help.buttons.goods'), 'need_help_goods')
  NEED_HELP_FURNITURE = inline_button(t('help.buttons.furniture'), 'need_help_furniture')
  NEED_HELP_EQUIPMENT = inline_button(t('help.buttons.equipment'), 'need_help_equipment')
  NEED_HELP_ELECTRONICS = inline_button(t('help.buttons.electronics'), 'need_help_electronics')
  NEED_HELP_FINANCIAL = inline_button(t('help.buttons.financial'), 'need_help_financial')
  NEED_HELP_CONTACT = inline_button(t('need_help.buttons.not_on_the_list'), 'contact')

  GIVE_HELP_PRODUCTS = inline_button(t('help.buttons.products'), 'give_help_products')
  GIVE_HELP_HOUSING = inline_button(t('help.buttons.housing'), 'give_help_housing')
  GIVE_HELP_GOODS = inline_button(t('help.buttons.goods'), 'give_help_goods')
  GIVE_HELP_FURNITURE = inline_button(t('help.buttons.furniture'), 'give_help_furniture')
  GIVE_HELP_EQUIPMENT = inline_button(t('help.buttons.equipment'), 'give_help_equipment')
  GIVE_HELP_ELECTRONICS = inline_button(t('help.buttons.electronics'), 'give_help_electronics')
  GIVE_HELP_FINANCIAL = inline_button(t('help.buttons.financial'), 'give_help_financial')
  GIVE_HELP_CONTACT = inline_button(t('give_help.buttons.not_on_the_list'), 'contact')

  GIVE_HELP_TO_WAR_VICTIMS = inline_button(t('give_help_whom.buttons.war_victims'), 'give_help_to_war_victims')
  GIVE_HELP_TO_WAR_DISABLE_PEOPLE =
    inline_button(t('give_help_whom.buttons.disabled_people'), 'give_help_to_disabled_people')
  GIVE_HELP_TO_CHILDREN = inline_button(t('give_help_whom.buttons.children'), 'give_help_to_children')
  GIVE_HELP_TO_PENSIONERS = inline_button(t('give_help_whom.buttons.pensioners'), 'give_help_to_pensioners')
  GIVE_HELP_TO_ORPHANS = inline_button(t('give_help_whom.buttons.orphans'), 'give_help_to_orphans')
  GIVE_HELP_TO_STUDENTS = inline_button(t('give_help_whom.buttons.students'), 'give_help_to_students')
  GIVE_HELP_TO_LARGE_FAMILIES = inline_button(t('give_help_whom.buttons.large_families'), 'give_help_to_large_families')
  GIVE_HELP_TO_ALL = inline_button(t('give_help_whom.buttons.all'), 'give_help_to_all')
  BACK_TO_GIVE_HELP_WHERE = inline_button(t('shared.buttons.back'), 'give_help_where')
end
