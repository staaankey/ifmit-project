require_relative 'application'

TelegramBot::Application.configure

require_relative '../app/models/all'
require_relative '../app/helpers/all'
require_relative '../app/assets/all'
require_relative '../app/responders/all'
