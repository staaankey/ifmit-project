module StandardMessages
  class << self
    def process
      case MessageHandler.message.text
      when '/start' then MainResponder.start
      else
        case MessageHandler.message.reply_to_message&.text
        when I18n.t('need_help_where.message')
          if MessageHandler.current_user.last_bot_message_id ==
             MessageHandler.message.reply_to_message&.message_id
            NeedHelpResponder.final
          end
        when I18n.t('give_help_where.message')
          if MessageHandler.current_user.last_bot_message_id ==
             MessageHandler.message.reply_to_message&.message_id
            GiveHelpResponder.whom
          end
        end
      end
    end
  end
end
