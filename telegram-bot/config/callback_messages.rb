module CallbackMessages
  class << self
    def process
      case MessageHandler.message.data
      when 'main_menu' then MainResponder.start
      when 'contact' then MainResponder.contact
      when 'need_help' then NeedHelpResponder.need_help
      when 'need_help_products',
           'need_help_housing',
           'need_help_goods',
           'need_help_furniture',
           'need_help_equipment',
           'need_help_electronics',
           'need_help_financial' then NeedHelpResponder.where
      when 'give_help' then GiveHelpResponder.give_help
      when 'give_help_where',
           'give_help_products',
           'give_help_housing',
           'give_help_goods',
           'give_help_furniture',
           'give_help_equipment',
           'give_help_electronics',
           'give_help_financial' then GiveHelpResponder.where
      when 'give_help_to_war_victims',
           'give_help_to_disabled_people',
           'give_help_to_children',
           'give_help_to_pensioners',
           'give_help_to_orphans',
           'give_help_to_students',
           'give_help_to_large_families',
           'give_help_to_all' then GiveHelpResponder.final
      end
    end
  end
end
