require 'telegram/bot'

require_relative '../lib/core/telegram_bot'

require_relative 'message_handler'
require_relative 'callback_messages'
require_relative 'standard_messages'
