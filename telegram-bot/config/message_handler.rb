class MessageHandler < TelegramBot::BasicHandler
  class << self
    attr_accessor :current_user
    protected :current_user=

    def process_message(message, bot)
      super(message, bot)

      return if old_message?

      self.current_user = User.find_or_create_by(uid: message.from.id)

      current_user.last_bot_message_id =
        case self.message
        when Telegram::Bot::Types::CallbackQuery
          (CallbackMessages.process&.dig('result', 'message_id') if secure?) ||
          current_user.last_bot_message_id
        when Telegram::Bot::Types::Message
          StandardMessages.process&.dig('result', 'message_id') ||
          current_user.last_bot_message_id
        end

      current_user.save
    end

    def callback_query?
      message.is_a?(Telegram::Bot::Types::CallbackQuery)
    end

    def message?
      message.is_a?(Telegram::Bot::Types::Message)
    end

    private

    def secure?
      (message.message.message_id == current_user.last_bot_message_id) ||
        current_user.last_bot_message_id.nil?
    end
  end
end
