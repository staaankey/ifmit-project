require_relative 'boot'

module HelpFastTelegramBot
  class Application < TelegramBot::Application
    config.i18n.available_locales = [:uk]
    config.i18n.default_locale = :uk

    def self.run
      Telegram::Bot::Client.run(config.telegram.token, logger: config.logger) do |bot|
        MessageHandler.bot_start_time = Time.now.to_i

        bot.listen do |message|
          MessageHandler.process_message(message, bot)
        end
      end
    end
  end
end
