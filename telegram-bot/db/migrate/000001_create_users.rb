class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.bigint :uid, null: false
      t.bigint :last_bot_message_id

      t.timestamps
    end
  end
end
