class AddDataToUser < ActiveRecord::Migration[7.0]
  def change
    change_table :users do |t|
      t.json :data, default: { role: nil, type: nil, city: nil, category: nil }
    end
  end
end
