require_relative 'application/database'
require_relative 'application/task_loader'

module TelegramBot
  class Application
    extend Dry::Configurable

    setting :root, **{
      default: Dir.pwd,
      constructor: proc { |value| Pathname(value) }
    }

    setting :env, **{
      default: ENV.fetch('APP_ENV', 'development')
    }

    setting :i18n, default: I18n

    setting :logger, **{
      default: ActiveSupport::Logger.new($stdout),
      reader: true
    }

    setting :telegram do
      setting :token, default: ENV.fetch('TELEGRAM_BOT_TOKEN', '')
    end

    class << self
      def run; end

      def database
        @database ||= Database.new.tap do |db|
          db.env    = config.env
          db.root   = config.root
          db.logger = config.logger
        end
      end

      def task_loader
        @task_loader ||= TaskLoader.new.tap do |tl|
          tl.root = config.root
          tl.database = database
        end
      end

      def configure
        database.configure_and_establish_connection
        configure_i18n
      end

      def load_tasks
        task_loader.load_all
      end

      private

      def configure_i18n
        config.i18n.load_path +=
          Dir.glob(
            File.join(config.root, 'config', 'locales', '*.yml')
          )
        config.i18n.backend.load_translations
      end
    end
  end
end
