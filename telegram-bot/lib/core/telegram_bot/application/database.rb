require_relative 'database/seed_loader'

module TelegramBot
  class Application
    class Database
      attr_accessor(
        :root,
        :logger
      )

      attr_writer(
        :env,
        :database_configuration,
        :db_dir,
        :fixtures_path,
        :migrations_paths,
        :seed_loader
      )

      def env
        @env ||= ENV.fetch('APP_ENV', 'development')
      end

      def database_configuration
        @database_configuration ||= Psych.load(
          ERB.new(
            File.read(
              File.join(root, 'config', 'database.yml')
            )
          ).result,
          aliases: true
        )
      end

      def db_dir
        @db_dir ||= File.join(root, 'db')
      end

      def fixtures_path
        @fixtures_path ||=
          if ENV['FIXTURES_PATH']
            File.join(root, ENV['FIXTURES_PATH'])
          else
            File.join(root, 'test', 'fixtures')
          end
      end

      def migrations_paths
        @migrations_paths ||= [File.join(db_dir, 'migrate')]
      end

      def seed_loader
        @seed_loader ||= SeedLoader.new(File.join(db_dir, 'seeds.rb'))
      end

      def establish_connection(config_or_env = nil)
        config_or_env ||= env.to_sym
        ActiveRecord::Base.establish_connection(config_or_env)
      end

      def configure_and_establish_connection
        configure
        establish_connection
      end

      def configure
        configure_active_record
        configure_tasks
      end

      def configure_active_record
        ActiveRecord::Base.tap do |config|
          config.configurations = database_configuration
          config.logger         = logger
        end
      end

      def configure_tasks
        ActiveRecord::Tasks::DatabaseTasks.tap do |config|
          config.env                    = env
          config.database_configuration = database_configuration
          config.db_dir                 = db_dir
          config.fixtures_path          = fixtures_path
          config.migrations_paths       = migrations_paths
          config.seed_loader            = seed_loader
          config.root                   = root
        end
      end

      def load_tasks
        load 'active_record/railties/databases.rake'
      end
    end
  end
end
