module TelegramBot
  class Application
    class TaskLoader
      attr_accessor :root, :database

      def load_all
        load_environment_task
        load_custom_tasks
        load_database_tasks
      end

      private

      def load_environment_task
        Rake::Task.define_task(:environment) {}
      end

      def load_custom_tasks
        Dir.glob(File.join(root, 'lib', 'tasks', '*.rake')).each do |rake_tasks|
          load rake_tasks
        end
      end

      def load_database_tasks
        database.load_tasks
      end
    end
  end
end
