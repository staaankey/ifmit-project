module TelegramBot
  class Application
    class Database
      class SeedLoader
        def initialize(seed_file)
          @seed_file = seed_file
        end

        def load_seed
          return unless File.file?(@seed_file)

          load @seed_file
        end
      end
    end
  end
end
