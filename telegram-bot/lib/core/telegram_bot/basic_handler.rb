module TelegramBot
  class BasicHandler
    class << self
      attr_accessor :message, :bot
      protected :message=, :bot=

      attr_writer :bot_start_time

      def process_message(message, bot)
        self.message = message
        self.bot = bot
      end

      protected

      def old_message?
        message_time = defined?(message.date) ? message.date : message.message.date
        message_time < @bot_start_time
      end
    end
  end
end
