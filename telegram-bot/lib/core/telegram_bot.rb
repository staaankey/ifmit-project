require 'active_record'
require 'active_support/all'
require 'dry-configurable'

require_relative 'telegram_bot/application'
require_relative 'telegram_bot/basic_handler'
