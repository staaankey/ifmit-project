CREATE TABLE "reports" (
    "id" SERIAL PRIMARY KEY NOT NULL,
    "title" varchar(255) NOT NULL,
    "description" varchar(255) NOT NULL,
    "user_id" integer NOT NULL
);

CREATE TABLE "users" (
     "id" SERIAL PRIMARY KEY NOT NULL,
     "full_name" VARCHAR(255) NOT NULL,
     "username" VARCHAR(255) NOT NULL,
     "password" varchar(255) NOT NULL,
     "phone" VARCHAR(255) NOT NULL,
     "email" VARCHAR(255) NOT NULL
);

ALTER TABLE "reports" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
