package group.staaankey.helpfast.repository;


import group.staaankey.helpfast.entity.User;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int registerUser(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL_INSERT = "insert into users (full_name, username, password, phone, email) " +
                "values (:fullName, :username, :password, :phone, :email)";

        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("fullName", user.getFullName())
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("phone", user.getPhone())
                .addValue("email", user.getEmail()), keyHolder, new String[] {"id"});

        return keyHolder.getKey().intValue();
    }
}
