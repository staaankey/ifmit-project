package group.staaankey.helpfast.repository;

import group.staaankey.helpfast.entity.Report;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class ReportRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public ReportRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int registerReport(Report report) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        final String SQL_INSERT = "insert into reports (title, description, user_id) values (:title, :description, :user_id)";
        jdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
                .addValue("title", report.getTitle())
                .addValue("description", report.getDescription())
                .addValue("user_id", report.getUserId()), keyHolder, new String[] {"id"});

        return keyHolder.getKey().intValue();
    }
}
