package group.staaankey.helpfast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpfastApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelpfastApplication.class, args);
    }

}
