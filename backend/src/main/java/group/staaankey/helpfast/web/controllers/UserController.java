package group.staaankey.helpfast.web.controllers;


import group.staaankey.helpfast.entity.User;
import group.staaankey.helpfast.service.UserService;
import group.staaankey.helpfast.web.dto.UserDto;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/users")
@CrossOrigin
public class UserController {
    private final UserService userServie;

    public UserController(UserService userServie) {
        this.userServie = userServie;
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody UserDto userDto) {
        return ResponseEntity.ok(userServie.registerUser(toEntity(userDto)));
    }

    private User toEntity(UserDto userDto) {
        var user = new User();
        BeanUtils.copyProperties(userDto, user);
        return user;
    }
}
