package group.staaankey.helpfast.web.controllers;


import group.staaankey.helpfast.entity.Report;
import group.staaankey.helpfast.service.ReportService;
import group.staaankey.helpfast.web.dto.ReportDto;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/reports")
@CrossOrigin
public class ReportController {
    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/create")
    public ResponseEntity registerReport(@RequestBody ReportDto reportDto) {
        return ResponseEntity.ok(reportService.registerReport(toEntity(reportDto)));
    }


    private Report toEntity(ReportDto reportDto) {
        var report = new Report();
        BeanUtils.copyProperties(reportDto, report);
        return report;
    }
}
