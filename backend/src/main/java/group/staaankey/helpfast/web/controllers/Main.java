package group.staaankey.helpfast.web.controllers;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class Main {
    @GetMapping("/hello")
    public String sayHello(){
        return "Hello, World!";
    }
}
