package group.staaankey.helpfast.service;

import group.staaankey.helpfast.entity.User;
import group.staaankey.helpfast.repository.UserRepository;
import org.springframework.stereotype.Service;


@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public int registerUser(User user) {
        return userRepository.registerUser(user);
    }
}
