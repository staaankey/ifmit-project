package group.staaankey.helpfast.service;

import group.staaankey.helpfast.entity.Report;
import group.staaankey.helpfast.repository.ReportRepository;
import org.springframework.stereotype.Service;

@Service
public class ReportService {
    private final ReportRepository reportRepository;

    public ReportService(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    public int registerReport(Report report) {
        return reportRepository.registerReport(report);
    }
}
